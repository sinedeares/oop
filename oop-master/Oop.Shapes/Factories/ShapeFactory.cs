﻿using System;

namespace Oop.Shapes.Factories
{
	public class ShapeFactory
	{
		public Shape CreateCircle(int radius)
		{
            if (radius > 0)
            {
                return new Circle(radius);
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Радиус должен быть положительным");
            }
            //throw new NotImplementedException();
        }

		public Shape CreateTriangle(int a, int b, int c)
		{
            if (a > 0 && b > 0 && c > 0)
            {
                if (a + b > c && a + c > b && b + c > a)
                {
                    return new Triangle(a, b, c);
                }
                else throw new InvalidOperationException($"Одна сторона треугольника должна быть меньше суммы двух других");
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Длина сторон треугольника должна быть положительной");
            }
            //throw new NotImplementedException();
		}

		public Shape CreateSquare(int a)
		{
            if (a > 0)
            {
                return new Square(a);
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Длина стороны квадрата должна быть положительной");
            }
			//throw new NotImplementedException();
		}

		public Shape CreateRectangle(int a, int b)
		{
            if (a > 0 && b > 0)
            {
                return new Rectangle(a, b);
            }
            else
            {
                throw new ArgumentOutOfRangeException($"Длины сторон прямоугольника должны быть положительными");
            }
            //throw new NotImplementedException();
        }
	}
}
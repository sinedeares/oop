﻿/*
Необходимо разработать иерархию классов геометрических фигур - окружность, треугольник, квадрат, прямоугольник.
Каждая фигура обладает следующими свойствами:
1. Площадь
2. Периметр
3. Количество вершин

Внешний доступ к этим свойствам осуществляется через переменную базового класса. Например:
Shape circle = new Circle();
var area = circle.Area;

Что необходимо сделать:
1. Создать классы, описывающие окружность, треугольник, квадрат, прямоугольник, которые являются дочерними для класса Shape.
2. Реализовать все методы в классе ShapeFactory.
3. Для реализации полиморфизма внести изменения в класс Shape, а также запретить создавать его экземпляры.
4. Для каждой фигуры переопределить метод IsEqual, который проверяет, равна ли текущая фигура другой, по следующим правилам:
	a. Окружность равна другой окружности с таким же радиусом.
	b. Треугольник равен другому треугольнику с такими же сторонами.
	с. Квадрат равен другому квадрату с такой же стороной.
	d. Прямоугольник равен другому прямоугольнику с такими же сторонами.

Весь функционал должен быть реализован в проекте Oop.Shapes.
Проект с юнит тестами изменять запрещено.
*/

using System;

namespace Oop.Shapes
{
	public abstract class Shape
	{
		/// <summary>
		/// Площадь фигуры
		/// </summary>
		public abstract double Area { get; }

		/// <summary>
		/// Периметр
		/// </summary>
		public abstract double Perimeter { get; }

		/// <summary>
		/// Количество вершин
		/// </summary>
		public abstract int VertexCount { get; }

		public virtual bool IsEqual(Shape shape) => false;
	}

	//ОКРУЖНОСТЬ
	public class Circle : Shape
    {
        public int radius;

        public Circle(int radius)
        {
            this.radius = radius;
        }
		public override double Area {
			get {
                if (radius > 0)
                {
                    return Math.PI * Math.Pow(radius, 2);
                }
                else
                {
                    throw new ArgumentOutOfRangeException();
                }
            }
		}

		public override double Perimeter => 2 * Math.PI * radius;

        public override int VertexCount => 0;


        public override bool IsEqual(Shape circle)
        {
			if (circle is Circle)
			{
				return this.radius == ((Circle)circle).radius;
			}
			return false;
        }

    }

	//ТРЕУГОЛЬНИК
	public class Triangle : Shape
	{
		private int a;
		private int b; 
		private int c;

        public Triangle(int a, int b, int c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
		}
		public override double Area => Math.Sqrt(Perimeter/2 * (Perimeter/2 - a) * (Perimeter/2 - b) * (Perimeter/2 - c));

        public override double Perimeter => a + b + c;

        public override int VertexCount => 3;
        public override bool IsEqual(Shape triangle)
		{
			
			if (triangle is Triangle)
			{
				bool expression1 = (this.a == ((Triangle)triangle).a) && (this.b == ((Triangle)triangle).b) && (this.c == ((Triangle)triangle).c);
				bool expression2 = (this.a == ((Triangle)triangle).b) && (this.b == ((Triangle)triangle).a) && (this.c == ((Triangle)triangle).c);
                bool expression3 = (this.a == ((Triangle) triangle).c) && (this.b == ((Triangle) triangle).b) && (this.c == ((Triangle) triangle).a);
                bool expression4 = (this.a == ((Triangle)triangle).a) && (this.b == ((Triangle)triangle).c) && (this.c == ((Triangle)triangle).b);
                bool expression5 = (this.a == ((Triangle)triangle).b) && (this.b == ((Triangle)triangle).c) && (this.c == ((Triangle)triangle).a);
                bool expression6 = (this.a == ((Triangle)triangle).c) && (this.b == ((Triangle)triangle).a) && (this.c == ((Triangle)triangle).b);
				return (expression1 || expression2 || expression3 || expression4 || expression5 || expression6);
			}
			return false;
		}
	}

	//КВАДРАТ
	public class Square : Shape
	{
		private int a;

        public Square(int a)
        {
            this.a = a;
        }
		public override double Area => Math.Pow(a,2);

        public override double Perimeter => a*4;

        public override int VertexCount => 4;

        public override bool IsEqual(Shape square)
		{
			if (square is Square)
			{
				return this.a == ((Square)square).a;
			}
			return false;
		}
	}


	//ПРЯМОУГОЛЬНИК
	public class Rectangle : Shape
    {
		private int a;
		private int b;

        public Rectangle(int a, int b)
        {
            this.a = a;
            this.b = b;
        }
		public override double Area => a * b;

        public override double Perimeter => (a + b)*2;

        public override int VertexCount => 4;

        public override bool IsEqual(Shape rectangle)
		{
			if (rectangle is Rectangle)
			{
				return (((this.a == ((Rectangle)rectangle).a) && (this.b == ((Rectangle)rectangle).b)) || ((this.a == ((Rectangle)rectangle).b) && (this.b == ((Rectangle)rectangle).a))) ;
			}
			return false;
		}
	}
}
